﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow_2 : MonoBehaviour {


    public float xMax = 100;
    public float yMax = 100;
    public float xMin = -100;
    public float yMin = -100;

    public float xValue = 0.165f;
    public float yValue = 0.16f;

    public Transform target;
    Transform t;
    public float mspeed = 0.1f;
    

    void Awake()
    {
        t = transform;
    }
   


	void Update () {
        
        if (target.transform.localScale.x > 0)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, 0.1f) + new Vector3(xValue, yValue, -10);
        }

        else if (target.transform.localScale.x < 0)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, 0.1f) + new Vector3(-xValue, yValue, -10);
        }

    }


    void LateUpdate()
    {
        float x = Mathf.Clamp(transform.position.x, xMin, xMax);
        float y = Mathf.Clamp(transform.position.y, yMin, yMax);
        t.position = new Vector3(x, y, t.position.z);
    }
}
