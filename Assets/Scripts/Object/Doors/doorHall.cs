﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorHall : MonoBehaviour {

    private Transform destination;
    public bool isOpen;
    

    void Start()
    {
        isOpen = false;       
    }

    

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isOpen = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player") && Input.GetKey(KeyCode.E))

        {           
            destination = GameObject.FindGameObjectWithTag("basementDoor").GetComponent<Transform>();
        }
        {
            other.transform.position = new Vector2(destination.position.x, destination.position.y);
        }
        {
            isOpen = false;
        }
        if (other.CompareTag("Player") && isOpen == false)
        {
            destination = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }
        {
            other.transform.position = new Vector2(destination.position.x, destination.position.y);
        }
    }
}
