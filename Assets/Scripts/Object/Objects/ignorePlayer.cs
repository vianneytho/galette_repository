﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ignorePlayer : MonoBehaviour {

    public GameObject ignoredObj;
    public GameObject ignorantObj;


	void Update()
    {
        Physics2D.IgnoreCollision(ignoredObj.GetComponent<Collider2D>(), ignorantObj.GetComponent<Collider2D>());
	}
		
}
