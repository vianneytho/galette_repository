﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {

    private PlatformEffector2D effector;
   


    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();

    }


    void Update()
    {

        if (Input.GetKey(KeyCode.S))
        {          
           effector.rotationalOffset = 180f;     
        }

    }

    void OnTriggerEnter2D(Collider2D other)

    {
        if (other.CompareTag("Player"))
        {
            effector.rotationalOffset = 0f;
        }
    }
}
