﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairs : MonoBehaviour {

    private PlatformEffector2D effector;
    

   
    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();

    }


    void Update()
    {

        if (Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.D))
        {
            effector.surfaceArc = 180f;
        }

    }

    void OnTriggerEnter2D(Collider2D other)

    {
        if (other.CompareTag("Player"))
        {
            effector.surfaceArc = 0f;
        }
    }
}

