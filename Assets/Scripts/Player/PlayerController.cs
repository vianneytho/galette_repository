﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float jumpForce;
   
    private Rigidbody2D rb;
    private Animator anim;
    private bool facingRight = true;

    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();        
    }
	
	
	void FixedUpdate()
    {
        {
            float horizontal = Input.GetAxis("Horizontal");

            HandleMovement(horizontal);
            Flip(horizontal);
        }     
    }

    void Update()
    {        
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
    }



    private void HandleMovement(float horizontal)
    {   
        // RUN SPEED/ANIM
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        anim.SetFloat("isRunning", Mathf.Abs(horizontal));

        // JUMP FORCE/INPUT/ANIM
        if (Input.GetKeyDown(KeyCode.Z) && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;
        }
    }



    void Flip(float horizontal)
    {
        // FLIP 
        if (facingRight == false && horizontal > 0 || facingRight == true && horizontal < 0)
        {
            facingRight = !facingRight;
            Vector3 Scaler = transform.localScale;
            Scaler.x *= -1;
            transform.localScale = Scaler;
        }

    }


}
