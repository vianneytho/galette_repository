﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitions : MonoBehaviour {


    public Animator transitionAnim;
    public string sceneName;




	void Update ()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(LoadScene());
        }

    }

    IEnumerator LoadScene()
    {
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(sceneName);
    }
}
